.. GitLab Pages with Sphinx documentation master file, created by
   sphinx-quickstart on Thu Jan  9 10:28:38 2020.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to HPC4L user guide and Wiki
====================================

.. toctree::
   :maxdepth: 0

   overview.md
   connect.md
   data_transfer.md
   jobs.md
   application_modules.md
   scientific_computing.md
   completed_projects.md
..   interactive_job.rst
   foo.md
   bar.md
