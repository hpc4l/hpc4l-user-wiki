# Configuration file for the Sphinx documentation builder.
#
# This file only contains a selection of the most common options. For a full
# list see the documentation:
# https://www.sphinx-doc.org/en/master/usage/configuration.html

# -- Path setup --------------------------------------------------------------

# If extensions (or modules to document with autodoc) are in another directory,
# add these directories to sys.path here. If the directory is relative to the
# documentation root, use os.path.abspath to make it absolute, like shown here.
#
import os
import sys
from sphinx.application import Sphinx
from typing import Any, Dict
# sys.path.insert(0, os.path.abspath('.'))


# -- Project information -----------------------------------------------------

project = "HPC4L Wiki "
copyright = "2024, GPL v3"
author = "HPC4L Team"

# The full version, including alpha/beta/rc tags
#release = 'https://gitlab.com/pages/sphinx'


# -- General configuration ---------------------------------------------------

# Add any Sphinx extension module names here, as strings. They can be
# extensions coming with Sphinx (named 'sphinx.ext.*') or your custom
# ones.
extensions = [
    'myst_parser',
    'sphinx.ext.todo',
]

# -- MyST options ------------------------------------------------------------

# This allows us to use ::: to denote directives, useful for admonitions
myst_enable_extensions = ["colon_fence", "linkify", "substitution"]
myst_heading_anchors = 2
myst_substitutions = {"rtd": "[Read the Docs](https://readthedocs.org/)"}

# -- Options for HTML output -------------------------------------------------

html_theme = "pydata_sphinx_theme"
html_logo = "_static/logo.svg"
html_favicon = "_static/logo.svg"
html_sourcelink_suffix = ""
html_last_updated_fmt = ""  # to reveal the build date in the pages meta

# Define the json_url for our version switcher.
#json_url = "https://pydata-sphinx-theme.readthedocs.io/en/latest/_static/switcher.json"
json_url = "https://pydata-sphinx-theme.readthedocs.io/en/latest/_static/switcher.json"

html_theme_options = {
    "external_links": [
    ],
    "header_links_before_dropdown": 4,
    "icon_links": [
        {
            "name": "X/Twitter",
            "url": "https://x.com/hpc4lebanon?lang=en",
            "icon": "fa-brands fa-twitter",
        },
        {
            "name": "Gitlab",
            "url": "https://gitlab.com/hpc4l",
            "icon": "fa-brands fa-github",
        },
        {
            "name": "Github",
            "url": "https://github.com/hpc4l",
            "icon": "fa-brands fa-github",
        },
        {
            "name": "HPC4L",
            "url": "https://hpc4l-webpage.web.cern.ch/",
            "icon": "_static/logo.svg",
            "type": "local",
            "attributes": {"target": "_blank"},
        },
    ],
    # alternative way to set twitter and github header icons
    # "github_url": "https://github.com/pydata/pydata-sphinx-theme",
    # "twitter_url": "https://twitter.com/PyData",
    "logo": {
        "text": "HPC4L wiki",
        "image_dark": "_static/logo.svg",
    },
    "use_edit_page_button": True,
    "show_toc_level": 2,
    "navbar_align": "left",  # [left, content, right] For testing that the navbar items align properly
    # "show_nav_level": 2,
    #"announcement": "https://raw.githubusercontent.com/pydata/pydata-sphinx-theme/main/docs/_templates/custom-template.html",
    "show_version_warning_banner": False,
    #"navbar_center": ["version-switcher", "navbar-nav"],
    # "navbar_start": ["navbar-logo"],
    # "navbar_end": ["theme-switcher", "navbar-icon-links"],
    # "navbar_persistent": ["search-button"],
    # "primary_sidebar_end": ["custom-template", "sidebar-ethical-ads"],
    # "article_footer_items": ["test", "test"],
    # "content_footer_items": ["test", "test"],
    "footer_start": ["copyright"],
    "footer_center": ["sphinx-version"],
    "secondary_sidebar_items": {
        "**/*": ["page-toc", "edit-this-page", "sourcelink"],
        "examples/no-sidebar": [],
    },
    "switcher": {
        "json_url": json_url,
        "version_match": '0.0.0',
    },
    "back_to_top_button": True,
}

html_sidebars = {
    '**': [
        'globaltoc.html',
        'relations.html',
    ],
}

#html_sidebars = {
#    "community/index": [
#        "sidebar-nav-bs",
#        "custom-template",
#    ],  # This ensures we test for custom sidebars
#}

#html_sidebars = {
#    "community/index": [
#        "sidebar-nav-bs",
#        "custom-template",
#    ],  # This ensures we test for custom sidebars
#    "examples/no-sidebar": [],  # Test what page looks like with no sidebar items
#    "examples/persistent-search-field": ["search-field"],
#    # Blog sidebars
#    # ref: https://ablog.readthedocs.io/manual/ablog-configuration-options/#blog-sidebars
#    "examples/blog/*": [
#        "ablog/postcard.html",
#        "ablog/recentposts.html",
#        "ablog/tagcloud.html",
#        "ablog/categories.html",
#        "ablog/authors.html",
#        "ablog/languages.html",
#        "ablog/locations.html",
#        "ablog/archives.html",
#    ],
#}

html_context = {
    "github_user": "pydata",
    "github_repo": "pydata-sphinx-theme",
    "github_version": "main",
    "doc_path": "docs",
}

#rediraffe_redirects = {
#    "contributing.rst": "community/index.rst",
#}

# Add any paths that contain custom static files (such as style sheets) here,
# relative to this directory. They are copied after the builtin static files,
# so a file named "default.css" will overwrite the builtin "default.css".
html_static_path = ["_static"]
html_css_files = ["custom.css"]
html_js_files = ["custom-icon.js"]
todo_include_todos = True

# -- favicon options ---------------------------------------------------------

# see https://sphinx-favicon.readthedocs.io for more information about the
# sphinx-favicon extension
favicons = [
    # generic icons compatible with most browsers
    "favicon-32x32.png",
    "favicon-16x16.png",
    {"rel": "shortcut icon", "sizes": "any", "href": "favicon.ico"},
    # chrome specific
    "android-chrome-192x192.png",
    # apple icons
    {"rel": "mask-icon", "color": "#459db9", "href": "safari-pinned-tab.svg"},
    {"rel": "apple-touch-icon", "href": "apple-touch-icon.png"},
    # msapplications
    {"name": "msapplication-TileColor", "content": "#459db9"},
    {"name": "theme-color", "content": "#ffffff"},
    {"name": "msapplication-TileImage", "content": "mstile-150x150.png"},
]


# Add any paths that contain templates here, relative to this directory.
templates_path = ['_templates']

# List of patterns, relative to source directory, that match files and
# directories to ignore when looking for source files.
# This pattern also affects html_static_path and html_extra_path.
exclude_patterns = ['_build', 'Thumbs.db', '.DS_Store']


# -- Options for HTML output -------------------------------------------------

# The theme to use for HTML and HTML Help pages.  See the documentation for
# a list of builtin themes.
#
html_theme = "pydata_sphinx_theme"

# Add any paths that contain custom static files (such as style sheets) here,
# relative to this directory. They are copied after the builtin static files,
# so a file named "default.css" will overwrite the builtin "default.css".
html_static_path = ['_static']

source_suffix = {
    '.rst': 'restructuredtext',
    '.txt': 'markdown',
    '.md': 'markdown',
}


def setup_to_main(
    app: Sphinx, pagename: str, templatename: str, context, doctree
) -> None:
    """Add a function that jinja can access for returning an "edit this page" link pointing to `main`."""

    def to_main(link: str) -> str:
        """Transform "edit on github" links and make sure they always point to the main branch.

        Args:
            link: the link to the github edit interface

        Returns:
            the link to the tip of the main branch for the same file
        """
        links = link.split("/")
        idx = links.index("edit")
        return "/".join(links[: idx + 1]) + "/main/" + "/".join(links[idx + 2 :])

    context["to_main"] = to_main


def setup(app: Sphinx) -> Dict[str, Any]:
    """Add custom configuration to sphinx app.

    Args:
        app: the Sphinx application
    Returns:
        the 2 parallel parameters set to ``True``.
    """
    app.connect("html-page-context", setup_to_main)

    return {
        "parallel_read_safe": True,
        "parallel_write_safe": True,
    }
