# Getting connected

## Connecting to HPC4L

### VPN

In order to connect to HPC4L you need to be connected to the HPC4L's VPN. We
recommend installing the openvpn client and use the credentials provided to you
when your account creation email was dispatched.

Openvpn is available for all major operating systems and can be downloaded from
the [openvpn website](https://openvpn.net/client/)

### Terminal access via SSH

After connecting to the vpn you can access the cluster via ssh:

```bash
ssh <username>@hpc4l.org
```

### Jupyterhub

After connecting to the vpn you can access the cluster via ssh:

You can access Jupyterhub by going to [https://jupyter.hpc4l.org](https://jupyter.hpc4l.org)
