==========================
Theme Structure and Layout
==========================
.. _Getting_started:

Getting connected
-----------------

Connecting to the cluster
=========================

In order to connect to the cluster, you need to be on the HPC4L network via
through the vpn server of HPC4L. After your account is created by the
administrators, you will receive an email with the details of your account
and how to connect to the vpn server.

.. todo::

    Add the details of the vpn server and how to connect to it for windows
    linux and mac users.

Getting connected 1
-------------------

aaaa

Getting connected 2
-------------------

aaaa


Connecting to a terminal
========================

When on the HPC4L vpn the following commands can be used to connect to the
gateway node of the cluster. The gateway node is the entry point to the cluster.

.. code-block:: bash

    ssh my_user_name@hpc4l.org

TIP: Passwordless login can be set up to avoid typing the password everytime and
     is safer than saving the password in the ssh client or re-typing it.

.. warning:: SECURITY: make sure to change your account password after the
 administrators have created your account. To change the account password
 after logging in, use the command ``passwd``

.. note:: direct ssh access to the compute nodes is disabled and not allowed.

Getting connected 1
-------------------

aaaa

Getting connected 2
-------------------

aaaa


Connecting to the jupyterhub service
====================================

The jupyterhub service is a web based service that allows you to run jupyter
notebooks on the cluster. The jupyterhub service is available at the following
url: https://hpc4l.org/jupyterhub/ and can be reached **only** after
connecting to the vpn server.
