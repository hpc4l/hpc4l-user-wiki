# Overview

## Organization

project website: https://hpc4l-webpage.web.cern.ch/

Lebanon joined the CERN family in 2016 through the signature of an International
Cooperation Agreement. This triggered a strong development of the country's
contributions to CERN projects, particularly through the affiliation of four of
its top universities to the Compact Muon Solenoid (CMS) experiment. Despite the
challenges occurring in Lebanon, the Lebanese scientific community has
reaffirmed its commitment to CERN and CMS.


## Hardware Resources

HPC4L is composed of 120 compute nodes and 8 storage servers. The total compute
capacity is:

  - 1440 physical cores and 3.75 TB of system DDR3 RAM
    - 120 compute nodes (servers), with the following specs:
      - 2x Intel Xeon L5640 CPUs @ 2.27GHz
        - 6 cores per CPU
      - 32 GB RAM per node
      - 700GB local disk
  - 1 PB of strage
  - The compute nodes are connected via 1 Gbit/s ethernet

## Target applications suitable for HPC4L

By design HPC4L is a general purpose High Throughput Computing (HTC) cluster,
this means that it is suitable for running applications that can be parallelized
across multiple nodes that are data input and output intensive that do not need
to communicate with each other during the computation. For such applications the
excpected throughput from the storage servers to the compute nodes can be up to
4 GB/s. Once the data is cached the aggregate disk throughput can be up to 12 GB/s.

HPC4L can be used and is suitable for compute intensive HPC applications that
**DO NOT** require low latency and high bandwidth (> 100MB/s+) communication over
the network.

HPC4L is **not** suitable for applications that require GPUs and high bandwidth
inter-node commuinication. That includes all application that require
CUDA / Tensorflow / PyTorch that require GPU acceleration. If the AI application is
small enough to be trained on a CPU then it can be run on HPC4L.

## Services

HPC4L provides the following services:

  - Slurm: A job scheduler that allows users to submit jobs to the cluster.
  - EOS: A distributed storage system that allows users to store and access
    data from the cluster.
  - Jupyterhub: A web application that allows users to run Jupyter notebooks on
    the cluster mainly for testing and development and not for production jobs.
  - NFS shares: A network file system that allows users to access their home
    directory from any node in the cluster.
  - Singularity/Apptainer: A container runtime that allows users to run
    applications in containers on the cluster.

## Support

### Getting an account

In order to get an account on HPC4L you need to be a member of the organization
within the HPC4L consortium or a member of a project that is using HPC4L. The
first steps to obtain an account is to request a project (see below), once the
project is approved your account will be provisioned.

## Requesting a project

### Submitting a proposal

To submit a proposal to use HPC4L supercomputer please fill out the
[project request form](./_static/docs/forms/HPC4L_Project_Request_Form.docx) and
email it to ``support@hpc4l.org`` to register your request.
The steps are the following:

  1. download the form.
  2. fill it out (if you need help filling the form please contact support.)
  3. export it as a pdf file (non pdf forms will be ignored).
  4. email it to ``support@hpc4l.org``.

:::{warning}
In order to contact support and/or request to join the HPC4L support gitlab project
please use your institutional email address.  Any emails from e.g ``.gmail.com``,
``.yahoo.com``, ``.hotmail.com`` or any other non-``.lb`` or non-acaedmic email
address will be ignored.
:::

## Acknowledgements
This project has been supported by the HPC4L IT and research computing support team, the HPC4L consortium members
and CERN where the simulations were run on the HPC cluster.
